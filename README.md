# JNode
<b>A tiny singe file, header only C89 library for parsing *.json files</b><br>

&emsp;<code style="font-weight: bold; background: rgba(0,0,0,0);">YES</code>&nbsp;
&emsp;I know, there are millions of JSON-parseres out there.<br>
&emsp;<code style="font-weight: bold; background: rgba(0,0,0,0);">NO&nbsp;</code>&nbsp;
&emsp;this parser is nothing special and not the best one for sure.<br>
&emsp;<code style="font-weight: bold; background: rgba(0,0,0,0);">BUT</code>&nbsp;
&emsp;it is my own attempt of writing a JSON-parser<br>
&emsp;<code style="font-weight: bold; background: rgba(0,0,0,0);">AND</code>&nbsp;
&emsp;I like it :blush:

---
<details>
<summary><b>Documentation:</b></summary>

---
<b>Types:</b>

<details>
<summary><code>JNode</code></summary>
&emsp;&emsp;&emsp;<code>JNodeType&nbsp;&nbsp;&nbsp;</code>&emsp;<b>type</b><br>
&emsp;&emsp;&emsp;<code>unsigned int</code>&emsp;<b>id</b><br>
&emsp;&emsp;&emsp;<code>unsigned int</code>&emsp;<b>size</b><br>
&emsp;&emsp;&emsp;<code>char*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code>&emsp;<b>name</b><br>
&emsp;&emsp;&emsp;<code>void*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</code>&emsp;<b>data</b><br>
</details>

<details>
<summary><code>JNodeType</code></summary>
&emsp;&emsp;&emsp;<code>JNODE_UNKNOWN</code>&emsp;<code><b>0x00</b></code><br>
&emsp;&emsp;&emsp;<code>JNODE_OBJECT&nbsp;</code>&emsp;<code><b>0x01</b></code><br>
&emsp;&emsp;&emsp;<code>JNODE_ARRAY&nbsp;&nbsp;</code>&emsp;<code><b>0x02</b></code><br>
&emsp;&emsp;&emsp;<code>JNODE_NULL&nbsp;&nbsp;&nbsp;</code>&emsp;<code><b>0x08</b></code><br>
&emsp;&emsp;&emsp;<code>JNODE_BOOL&nbsp;&nbsp;&nbsp;</code>&emsp;<code><b>0x18</b></code><br>
&emsp;&emsp;&emsp;<code>JNODE_INT&nbsp;&nbsp;&nbsp;&nbsp;</code>&emsp;<code><b>0x28</b></code><br>
&emsp;&emsp;&emsp;<code>JNODE_FLOAT&nbsp;&nbsp;</code>&emsp;<code><b>0x38</b></code><br>
&emsp;&emsp;&emsp;<code>JNODE_STRING&nbsp;</code>&emsp;<code><b>0x48</b></code><br>
</details>




<b>Settings:</b>

<details>
<summary><code>JNODE_MAX_PATH_LENGTH</code></summary>

* <b>Description:</b><br>
Defines the maximum path length that can be requested with the getter-functions.<br>
Usage is <u>optional</u>. Needs to be defined <code>#define</code> <u>before</u> library is included.

* <b>Default:</b>&emsp;256
</details>

<details>
<summary><code>JNODE_MAX_STACK_SIZE</code></summary>

* <b>Description:</b><br>
Defines the maximum depth a node can be nested. (node in node, in node...)<br>
Usage is <u>optional</u>. Needs to be defined with <code>#define</code> <u>before</u> library is included.

* <b>Default:</b>&emsp;64
</details>




<b>Parsing:</b>
<details>
<summary><code>JNode* JNode_loadJSON( const char* filename )</code></summary>

* <b>Description:</b><br>
loads and parses a *.json file

* <b>Parameters:</b><br>
<code>filename</code><br>This is the C string containing the name of the *json file to be opened.

* <b>Return Value:</b><br>
This function returns a <code>JNode</code> pointer. If unable to open or parse file <code>NULL</code> is returned.
</details>

<details>
<summary><code>JNode* JNode_loadJSONData( char* data )</code></summary>

* <b>Description:</b><br>
loads and parses a json C string

* <b>Parameters:</b><br>
<code>data</code><br>This is the C string containing the json data.

* <b>Return Value:</b><br>
This function returns a <code>JNode</code> pointer. If unable to parse data <code>NULL</code> is returned.
</details>






<b>Interface:</b>
<details>
<summary><code>JNode* JNode_getNode( JNode* node, const char *format, ... )</code></summary>

* <b>Description:</b><br>
Searches a child node described in the C string pointed by format. If format includes format specifiers (subsequences beginning with %), the additional arguments following format are formatted and inserted in the resulting string replacing their respective specifiers. Works like <code>printf</code> from ``stdio.h``.

* <b>Parameters:</b><br>
<code>node</code><br>
Pointer to parent node to be searched in. The search path is interpreted realtive to this node<br>
Only OBJECT- and ARRAY-nodes are able to contain child nodes.<br>
&nbsp;<br>
<code>format</code><br>
This is the C string containing the path of the child node. The search string is needs to be in C notation style:<br>
<i>prototype:</i> ``"object.object.array[index].object..."``<br>
&nbsp;<br>
<code>... (additional arguments)</code><br>
Depending on the format string, the function may expect a sequence of additional arguments, each containing a value to be used to replace a format specifier in the format string (or a pointer to a storage location, for n).
There should be at least as many of these arguments as the number of values specified in the format specifiers. Additional arguments are ignored by the function.

* <b>Return Value:</b><br>
This function returns a <code>JNode</code> pointer. If unable to find child node <code>NULL</code> is returned.
</details>


<details>
<summary><code>JNode* JNode_getBool( JNode* node, const char *format, ... )</code></summary>

* <b>Description:</b><br>
Searches a BOOL-node described in the C string pointed by format.<br>
See ``JNode_getNode`` for more information.

* <b>Parameters:</b><br>
<code>node</code><br>
Pointer to parent node to be searched in. The search path is interpreted realtive to this node<br>
Only <b>non</b> OBJECT- or ARRAY-nodes are able to contain primitive data.<br>
&nbsp;<br>
<code>format</code><br>
This is the C string containing the path of the child node.<br>
See ``JNode_getNode`` for more information.<br>
&nbsp;<br>
<code>... (additional arguments)</code><br>
See ``JNode_getNode`` for more information.

* <b>Return Value:</b><br>
This function returns a <code>unsiged char</code> which is either 0 or 1.<br>
INT- and FLOAT-nodes will be reinterpreted to 0 or not 0.<br>
STRING-nodes will be reinterpreted to 1 if filled or 0 if empty.
</details>



<details>
<summary><code>JNode* JNode_getInt( JNode* node, const char *format, ... )</code></summary>

* <b>Description:</b><br>
Searches a INT-node described in the C string pointed by format.<br>
See ``JNode_getNode`` for more information.

* <b>Parameters:</b><br>
<code>node</code><br>
Pointer to parent node to be searched in. The search path is interpreted realtive to this node<br>
Only <b>non</b> OBJECT- or ARRAY-nodes are able to contain primitive data.<br>
&nbsp;<br>
<code>format</code><br>
This is the C string containing the path of the child node.<br>
See ``JNode_getNode`` for more information.<br>
&nbsp;<br>
<code>... (additional arguments)</code><br>
See ``JNode_getNode`` for more information.

* <b>Return Value:</b><br>
This function returns a <code>int</code> containing the number stored in the child node.<br>
BOOL- and FLOAT-nodes will be reinterpreted to an integer.<br>
STRING-nodes will return their length.
</details>


<details>
<summary><code>JNode* JNode_getFloat( JNode* node, const char *format, ... )</code></summary>

* <b>Description:</b><br>
Searches a FLOAT-node described in the C string pointed by format.<br>
See ``JNode_getNode`` for more information.

* <b>Parameters:</b><br>
<code>node</code><br>
Pointer to parent node to be searched in. The search path is interpreted realtive to this node<br>
Only <b>non</b> OBJECT- or ARRAY-nodes are able to contain primitive data.<br>
&nbsp;<br>
<code>format</code><br>
This is the C string containing the path of the child node.<br>
See ``JNode_getNode`` for more information.<br>
&nbsp;<br>
<code>... (additional arguments)</code><br>
See ``JNode_getNode`` for more information.

* <b>Return Value:</b><br>
This function returns a <code>float</code> containing the number stored in the child node.<br>
BOOL- and INT-nodes will be reinterpreted to a float.<br>
STRING-nodes will return their length.
</details>


<details>
<summary><code>JNode* JNode_getString( JNode* node, const char *format, ... )</code></summary>

* <b>Description:</b><br>
Searches a STRING-node described in the C string pointed by format.<br>
See ``JNode_getNode`` for more information.

* <b>Parameters:</b><br>
<code>node</code><br>
Pointer to parent node to be searched in. The search path is interpreted realtive to this node<br>
Only <b>non</b> OBJECT- or ARRAY-nodes are able to contain primitive data.<br>
&nbsp;<br>
<code>format</code><br>
This is the C string containing the path of the child node.<br>
See ``JNode_getNode`` for more information.<br>
&nbsp;<br>
<code>... (additional arguments)</code><br>
See ``JNode_getNode`` for more information.

* <b>Return Value:</b><br>
This function returns a <code>const char</code> pointer, pointing to the C string stored in the child node.<br>
BOOL-, INT- and FLOAT nodes will be reinterpreted to a empty string.
</details>



<details>
<summary><code>const char* JNode_getTypeName( JNodeType type )</code></summary>

* <b>Description:</b><br>
This function converts a <code>JNodeType</code> to a C string.

* <b>Parameters:</b><br>
<code>type</code><br>
<code>type</code>-attribute of a node.

* <b>Return Value:</b><br>
This function returns <code>const char</code> pointer.
</details>





<b>Free:</b>
<details>
<summary><code>void JNode_free( JNode* node )</code></summary>

* <b>Description:</b><br>
Deletes a node and all its child nodes

* <b>Parameters:</b><br>
<code>node</code><br>Node pointer, pointing to the node to be deleted.

* <b>Return Value:</b><br>
nothing
</details>








---
</details>





<details>
<summary><b>Example:</b></summary>

---

```c
#include "../jnode.h"                                                                      
																										
int main( void ){                                                                       
																						
	JNode* json = JNode_loadJSON( "example.json" );                                        
	if( !json ){ return 1; }                                                            
																						
	printf( "%s %s\n",                                                                  
		JNode_getString( json, "node1.array1[%i]", 1 ),                                      
		JNode_getString( json, "node1.array2[%i]", 1 )                                       
	);                                                                                  

	JNode_free( json );

	return 0;                                                                           
} 
```


```
foo bar
```
---
</details>





<details>
<summary><b>Benchmark:</b></summary>

---
&emsp;<code style="font-weight: bold; background: rgba(0,0,0,0);">CPU&nbsp;</code>
&emsp;Intel® Core™ i7-4700MQ CPU - 2.40 GHz (using only one core)<br>
&emsp;<code style="font-weight: bold; background: rgba(0,0,0,0);">RAM&nbsp;</code>
&emsp;16GB DDR3 - 1600 MT/s<br>
&emsp;<code style="font-weight: bold; background: rgba(0,0,0,0);">FILE</code>
&emsp;JSON - 189,8 MB - 13'805'884 nodes

```
parsing:
  - 189778220 Bytes (189.8 MB)
  - time: 1770086 μs (107 MB/s)

interfacing:
  - features[1654].type => "Feature"
  - time: 2 μs

free:
  - deleted 13805884 nodes
  - time: 560925 μs
```
---
</details>




---
<b>Copyright 2021 Oliver Engelhardt</b>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

---

