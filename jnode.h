#ifndef JNODE_H
#define JNODE_H 1
/*
   Copyright 2021 Oliver Engelhardt

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/**#####################################################################################################
       _ _   _           _      
      | | \ | |         | |           (c) 06/2021 Oliver Engelhardt
      | |  \| | ___   __| | ___ 
  _   | | . ` |/ _ \ / _` |/ _ \      A tiny singe file, header only C89 lib for parsing *.json files
 | |__| | |\  | (_) | (_| |  __/      
  \____/|_| \_|\___/ \__,_|\___|      DO NOT USE IN PRODUCTION - UNLESS YOU TESTED IT PROPERLY!!!

#####################################################################################################**/
	
	#include <stdio.h>
	#include <stdlib.h>
    #include <stdarg.h>
	#include <string.h>
	
	/// TYPES ======================================================================================
		typedef unsigned char JNodeType;                                                                
		#define JNODE_UNKNOWN 		0x00                                                                
		#define JNODE_OBJECT   		0x01                                                                
		#define JNODE_ARRAY   		0x02                                                                
																										
		#define JNODE_NULL  		0x08                                                                
		#define JNODE_BOOL  		0x18                                                                
		#define JNODE_INT     		0x28                                                                
		#define JNODE_FLOAT 		0x38                                                                
		#define JNODE_STRING  		0x48   
		
		#define JNode_getTypeName( t ) \
			t==0x01?"OBJECT": \
			t==0x02?"ARRAY": \
			t==0x08?"NULL": \
			t==0x18?"BOOL": \
			t==0x28?"INT": \
			t==0x38?"FLOAT": \
			t==0x48?"STRING": \
			"UNKNOWN"

		typedef struct{
			JNodeType type;
			unsigned int id;
			unsigned int size;
			char* name;
			void* data;
		} JNode;
		
	/// PARSER =====================================================================================
		
		JNode* JNode_loadJSON     ( const char* filename );
		JNode* JNode_loadJSONData ( char* data );
		
	/// ============================================================================================
					
		/// CASES
			#define _JNODE_caseWhiteSpace \
				case ' ': case '\n': case '\t': case '\r'
				
			#define _JNODE_caseObject \
				case '{'
				
			#define _JNODE_caseObjectEnd \
				case '}'
				
			#define _JNODE_caseArray \
				case '['
				
			#define _JNODE_caseArrayEnd \
				case ']'
				
			#define _JNODE_caseString \
				case '"'
				
			#define _JNODE_caseDot \
				case '.'

			#define _JNODE_caseNumber \
				case '0': case '1': case '2': case '3': case '4': \
				case '5': case '6': case '7': case '8': case '9'
				
			#define _JNODE_caseNumberEx \
				case '0': case '1': case '2': case '3': case '4': \
				case '5': case '6': case '7': case '8': case '9': \
				case '+': case '-'
				
			#define _JNODE_caseHexNumber \
				case '0': case '1': case '2': case '3': case '4': \
				case '5': case '6': case '7': case '8': case '9': \
				case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': \
				case 'a': case 'b': case 'c': case 'd': case 'e': case 'f'
									
			#define _JNODE_caseBoolean \
				case 'f': case 't'
											
			#define _JNODE_caseNull \
				case 'n'
					
			#define _JNODE_boolean_false \
				(*(data)=='f' && *(data+1)=='a' && *(data+2)=='l' && *(data+3)=='s' && *(data+4)=='e') 

			#define _JNODE_boolean_true \
				(*(data)=='t' && *(data+1)=='r' && *(data+2)=='u' && *(data+3)=='e') 
					
			#define _JNODE_null \
				(*(data)=='n' && *(data+1)=='u' && *(data+2)=='l' && *(data+3)=='l')
					
		/// NODE							
			#define _JNODE_setDataString( str, len ) \
				stack[ stackPos ]->data = (void*)malloc( len+1 ); \
				memcpy( stack[ stackPos ]->data, str, len ); \
				((char*)stack[ stackPos ]->data)[len] = 0; \
				stack[ stackPos ]->size = len+1; \
				stack[ stackPos ]->type = JNODE_STRING; 			
			
			#define _JNODE_setDataInt( num ) \
				stack[ stackPos ]->data = (void*)malloc( 4 ); \
				*((int*)_JNODE_stack->data) = num; \
				stack[ stackPos ]->size = 4; \
				stack[ stackPos ]->type = JNODE_INT; 
				
			#define _JNODE_setDataFloat( num ) \
				stack[ stackPos ]->data = (void*)malloc( 4 ); \
				*((float*)_JNODE_stack->data) = num; \
				stack[ stackPos ]->size = 4; \
				stack[ stackPos ]->type = JNODE_FLOAT;
				
			#define _JNODE_setDataBoolean( num ) \
				stack[ stackPos ]->data = (void*)malloc( 1 ); \
				*((unsigned char*)_JNODE_stack->data) = num; \
				stack[ stackPos ]->size = 1; \
				stack[ stackPos ]->type = JNODE_BOOL;
				
			#define _JNODE_setDataNull() \
				stack[ stackPos ]->data = 0; \
				stack[ stackPos ]->size = 0; \
				stack[ stackPos ]->type = JNODE_NULL;
				
		/// FILE PARSING
			#define _JNODE_skipWhiteSpace() \
				for(;;){ switch( *data ){ _JNODE_caseWhiteSpace: data++; continue; } break; }
				
			#define _JNODE_skipToNext( chr ) \
				for(;;){ switch( *data ){ case chr: case 0: break; default: data++; continue; } break; } 

			#define _JNODE_parseString( p, l ) \
				p = ++data; _JNODE_skipToNext( '"' ); \
				l = (int)( (data++)-p );
				
			#define _JNODE_parseSign( s ) \
				switch( *data ){ \
					case '-':	data++; _JNODE_skipWhiteSpace(); s = -1; break; \
					case '+':	data++; _JNODE_skipWhiteSpace(); \
					default: 	s = 1; \
				}
				
			#define _JNODE_parseHexNumber( nr ) \
				data+=2; nr = 0; \
				for(;;){ switch( *data ){ \
					_JNODE_caseHexNumber: nr = (nr<<4) | ((*data < 'A') ? *data & 0xF : (*data & 0x7) + 9); data++; continue; \
				} break; } \
				nr *= i;
				
			#define _JNODE_parseNumber( nr, flt ) \
				int d = 0; nr = 0; isFloat = 0; \
				for(;;){ switch( *data ){\
					_JNODE_caseNumber:	if( isFloat ){ d = d*10 + (*(data++))-48; isFloat*=10; } \
										else{ nr = nr * 10 + (*(data++))-48; } continue; \
					_JNODE_caseDot: 	isFloat = 1; data++; continue; \
				} break; } \
				if( isFloat ){ flt = (float)i * ((float)nr + (float)d/(float)isFloat); } \
				else{ nr *= i; }					

		/// STACK
			#ifndef JNODE_MAX_STACK_SIZE
				#define JNODE_MAX_STACK_SIZE 64
			#endif
		
			#define _JNODE_stackInit() \
				JNode* stack[ JNODE_MAX_STACK_SIZE ]; \
				int stackPos = -1;
				
			#define _JNODE_stack \
				stack[ stackPos ]
				
			#define _JNODE_stackPush( typ ) \
				stackPos++; \
				stack[ stackPos ] = (JNode*)malloc( _JNODE_SIZE ); \
				memset( stack[ stackPos ], 0, _JNODE_SIZE ); \
				stack[ stackPos ]->type = typ; \
				if( nameLen ){ \
					stack[ stackPos ]->name = (char*)malloc(nameLen+1); \
					memcpy( stack[ stackPos ]->name, name, nameLen ); \
					stack[ stackPos ]->name[nameLen] = 0; \
					stack[ stackPos ]->id = _JNODE_hash( stack[ stackPos ]->name ); \
					nameLen = 0; \
				}				
			
			#define _JNODE_stackPop() \
				stackPos--; \
				if( stackPos>=0 ){ \
					stack[ stackPos ]->data = (void*)(stack[ stackPos ]->size? \
						realloc( stack[ stackPos ]->data, _JNODEPTR_SIZE*(stack[ stackPos ]->size+1) ):\
						malloc( _JNODEPTR_SIZE*(stack[ stackPos ]->size+1) ) \
					); \
					((JNode**)stack[ stackPos ]->data)[ stack[ stackPos ]->size++ ] = stack[ stackPos+1 ]; \
				}
				
		/// HASHING
			unsigned int _JNODE_hash( const char* data ){
				int dataLen = strlen( data );
				int offset = 0;
				unsigned int xxH32;
				
				if( dataLen >= 16 ){
					const unsigned int limit = dataLen - 16;
					unsigned int v1 = 1989 + 606290984U;
					unsigned int v2 = 1989 + 2246822519U;
					unsigned int v3 = 1989;
					unsigned int v4 = 1989 - 2654435761U;
					do{
						v1 += *(unsigned int*)( data+offset ) * 2246822519U;
						v1 = ((v1<<13)|(v1>>19)) * 2654435761U;
						offset += 4;        
						v2 += *(unsigned int*)( data+offset ) * 2246822519U;
						v2 = ((v2<<13)|(v2>>19)) * 2654435761U;
						offset += 4;        
						v3 += *(unsigned int*)( data+offset ) * 2246822519U;
						v3 = ((v3<<13)|(v3>>19)) * 2654435761U;
						offset += 4;        
						v4 += *(unsigned int*)( data+offset ) * 2246822519U;
						v4 = ((v4<<13)|(v4>>19)) * 2654435761U;
						offset += 4;			
					}
					while( offset <= limit );      
					xxH32 = ((v1<<1)|(v1>>31))+((v2<<7)|(v2>>25))+((v3<<12)|(v3>>20))+((v4<<18)|(v4>>14));
				} 
				else{ xxH32 = 1989 + 374761393U; }
				
				for( xxH32 += dataLen; offset <= dataLen - 4; offset += 4 ){
					xxH32 += *(unsigned int*)(data+offset) * 3266489917U;
					xxH32 = ((xxH32<<17)|(xxH32>>15)) * 668265263U;
				}
				while( offset < dataLen ){
					xxH32 += data[offset] * 374761393U;
					xxH32 = ((xxH32<<11)|(xxH32>>21)) * 2654435761U;
					++offset;
				}

				xxH32 = (xxH32^(xxH32 >> 15)) * 2246822519U;
				xxH32 = (xxH32^(xxH32 >> 13)) * 3266489917U;
				xxH32 = (xxH32^(xxH32 >> 16));
				
				return xxH32;
			}
				
		/// FUNCTIONS
			JNode* JNode_loadJSONData( char* data ){
				if( !data || !(*data) ){ return 0; }
				
				static const unsigned int _JNODE_SIZE = sizeof( JNode );
				static const unsigned int _JNODEPTR_SIZE = sizeof( JNode* );
				_JNODE_stackInit();
				
				char* ptr;
				char* name; int nameLen = 0;
				unsigned int isFloat; float f;
				int i, n;
				
				do{ _JNODE_skipWhiteSpace();				
					switch( *data ){					
						_JNODE_caseObject:		_JNODE_stackPush( JNODE_OBJECT ); break;
						_JNODE_caseObjectEnd:	_JNODE_stackPop(); break;
						
						_JNODE_caseArray:		_JNODE_stackPush( JNODE_ARRAY ); break;					
						_JNODE_caseArrayEnd:	_JNODE_stackPop(); break;
							
						_JNODE_caseString:		_JNODE_parseString( ptr, i );
												_JNODE_skipWhiteSpace();
												if( *data==':' ){ name = ptr; nameLen = i; continue; }
												else{ data--; }
												
												_JNODE_stackPush( JNODE_STRING );
												_JNODE_setDataString( ptr, i );
												_JNODE_stackPop(); break;
												
						_JNODE_caseNumberEx:	_JNODE_parseSign( i );
												if( *data=='0' && *(data+1)=='x' ){ 
													_JNODE_parseHexNumber( n );
													_JNODE_stackPush( JNODE_INT );
													_JNODE_setDataInt( n );	}
												else{ 
													_JNODE_parseNumber( n, f );
													if( isFloat ){
														_JNODE_stackPush( JNODE_FLOAT );
														_JNODE_setDataFloat( f );
													}else{
														_JNODE_stackPush( JNODE_INT );
														_JNODE_setDataInt( n );
													}
												}
												_JNODE_stackPop(); break;
												
						_JNODE_caseBoolean:		if( _JNODE_boolean_false ){ data+=5;
													_JNODE_stackPush( JNODE_BOOL );
													_JNODE_setDataBoolean( 0 );
													_JNODE_stackPop(); } else
													
												if( _JNODE_boolean_true ){ data+=4;
													_JNODE_stackPush( JNODE_BOOL );
													_JNODE_setDataBoolean( 1 );
													_JNODE_stackPop(); } break;
						
						_JNODE_caseNull:		if( _JNODE_null ){ data+=4; 
													_JNODE_stackPush( JNODE_NULL );
													_JNODE_setDataNull();
													_JNODE_stackPop(); } break;
					}
				} while( stackPos>=0 && *(++data) );
				
				stackPos = 0;
				return _JNODE_stack;
			}
			
			JNode* JNode_loadJSON( const char* filename ){
				FILE* file = fopen( filename, "rb" );
					if( !file ){ return 0; }
					fseek( file, 0, SEEK_END );
					unsigned int fileSize = ftell( file );
					fseek( file, 0, SEEK_SET );
					char* fileData = (char*)malloc( fileSize );
					fread( fileData, fileSize, 1, file );
				fclose( file );
				
				JNode* node = JNode_loadJSONData( fileData );

				free( fileData );
				return node;
			}
			
	/// INTERFACE ==================================================================================
		
		JNode*        JNode_getNode   ( JNode* node, const char *format, ... );
		unsigned char JNode_getBool   ( JNode* node, const char *format, ... );
		int           JNode_getInt    ( JNode* node, const char *format, ... );
		float         JNode_getFloat  ( JNode* node, const char *format, ... );
		const char*   JNode_getString ( JNode* node, const char *format, ... );
		
	/// ============================================================================================
	
		/// PATH PARSING
			#ifndef JNODE_MAX_PATH_LENGTH
				#define JNODE_MAX_PATH_LENGTH 256
			#endif
			
			char _JNODE_PATH_BUFFER[ JNODE_MAX_PATH_LENGTH ];
		
			#define _JNODE_INTERFACE_cutPath() \
				*(path) = 0; \
				sub = ptr; \
				ptr = ++path;
				
			#define _JNODE_INTERFACE_parseArgs( str ) \
				va_list arg; \
				va_start( arg, format ); \
				vsprintf( _JNODE_PATH_BUFFER, format, arg ); \
				va_end( arg ); \
				char *str = (char*)_JNODE_PATH_BUFFER;
							
			#define _JNODE_INTERFACE_parseNumber( nr ) \
				nr = 0; \
				for(;;){ switch( *path ){ \
					_JNODE_caseNumber: nr = nr*10 + (*(path))-48; path++; continue; \
				} path++; break; } ptr = ++path;
		
			#define _JNODE_INTERFACE_findNodeStr( str ) \
				if( node->type!=JNODE_OBJECT ){ return 0; } \
				found = 0; \
				id = _JNODE_hash( str ); \
				for( i=0; i<(int)node->size; i++ ){ \
					if( ((JNode**)node->data)[i]->id==id ){ node = ((JNode**)node->data)[i]; found = 1; break; } \
				} \
				if( !found ){ return 0; }
				
			#define _JNODE_INTERFACE_findNodeNum( nr ) \
				if( node->type!=JNODE_ARRAY || node->size<=nr ){ return 0; } \
				node = ((JNode**)node->data)[nr];
				
			#define _JNODE_INTERFACE_findPath( n ) \
				if( !node ){ return 0; } \
				_JNODE_INTERFACE_parseArgs( path );	\
				JNode* n = _JNODE_INTERFACE_getNode( node, path ); \
				if( !n || !n->size || !(n->type&8) ){ return 0; }
				
		/// FUNCTIONS
			JNode* _JNODE_INTERFACE_getNode( JNode* node, char *path ){								
				char *ptr = path;
				char *sub = path;
				unsigned int id, i;
				unsigned char found;
				
				for(;;){ switch( *path ){
					case '[':	_JNODE_INTERFACE_cutPath();
								_JNODE_INTERFACE_findNodeStr( sub );								
								_JNODE_INTERFACE_parseNumber( id );
								_JNODE_INTERFACE_findNodeNum( id ); continue;
					
					case '.': 	_JNODE_INTERFACE_cutPath();
								_JNODE_INTERFACE_findNodeStr( sub ); continue;
								
					case 0: 	if( !(*ptr) ){ break; }
								_JNODE_INTERFACE_cutPath();
								_JNODE_INTERFACE_findNodeStr( sub ); break;
					
					default: 	path++; continue;					
				} break; }
				
				return node;
			};

			JNode* JNode_getNode( JNode* node, const char *format, ... ){
				if( !node ){ return 0; }
				_JNODE_INTERFACE_parseArgs( path );
				return _JNODE_INTERFACE_getNode( node, path );
			}

			unsigned char JNode_getBool( JNode* node, const char *format, ... ){
				_JNODE_INTERFACE_findPath( out );
				switch( out->type ){
					case JNODE_BOOL:	return (*((unsigned char*)out->data))!=0?1:0;
					case JNODE_INT:		return (*((int*)out->data))!=0?1:0;
					case JNODE_FLOAT:	return ((int)(*((float*)out->data)))!=0?1:0;
					case JNODE_STRING:  return out->size!=0?1:0;
				}
				return 0;
			}

			int JNode_getInt( JNode* node, const char *format, ... ){
				_JNODE_INTERFACE_findPath( out );
				switch( out->type ){
					case JNODE_INT:		return *((int*)out->data);
					case JNODE_FLOAT:   return (int)(*((float*)out->data));
					case JNODE_BOOL:    return (*((unsigned char*)out->data))!=0?1:0;
					case JNODE_STRING:  return out->size;
				}
				return 0;
			}
			
			float JNode_getFloat( JNode* node, const char *format, ... ){
				_JNODE_INTERFACE_findPath( out );
				switch( out->type ){
					case JNODE_FLOAT:	return (*((float*)out->data));
					case JNODE_INT:		return (float)(*((int*)out->data));
					case JNODE_BOOL:	return (*((unsigned char*)out->data))!=0?1.0f:0.0f;
					case JNODE_STRING:	return (float)out->size;
				}
				return 0;
			}
			
			const char* JNode_getString( JNode* node, const char *format, ... ){
				if( !node ){ return 0; }
				_JNODE_INTERFACE_parseArgs( path );
				JNode* out = _JNODE_INTERFACE_getNode( node, path );
				if( !out || !out->size || out->type!=JNODE_STRING ){ return ""; }
				return (const char*)out->data;
			}
		
	/// FREE =======================================================================================
		
		int JNode_free( JNode* node );
		
	/// ============================================================================================
		
		/// STACK
			#define _JNODE_FREE_stack \
				_JNODE_stack
		
			#define _JNODE_FREE_stackInit() \
				_JNODE_stackInit()
			
			#define _JNODE_FREE_stackPush( n ) \
				stack[ ++stackPos ] = n; \
				stack[ stackPos ]->id = 0;
				
			#define _JNODE_FREE_stackPop() \
				if( stack[ stackPos ]->name ){ free( stack[ stackPos ]->name ); } \
				if( stack[ stackPos ]->data ){ free( stack[ stackPos ]->data ); } \
				free( stack[ stackPos-- ] ); \
				notesCount++;
				
			#define _JNODE_FREE_nextChild \
				((JNode**)_JNODE_FREE_stack->data)[ _JNODE_FREE_stack->id++ ]
		
		/// FUNCTIONS
			int JNode_free( JNode* node ){
				if( !node ){ return 0; }
				int notesCount = 0;
				_JNODE_FREE_stackInit();
				_JNODE_FREE_stackPush( node );	
				do{
					if( (_JNODE_FREE_stack->type&8) || !_JNODE_FREE_stack->size ){ 
						_JNODE_FREE_stackPop(); 
						continue; 
					}
					_JNODE_FREE_stack->size--;
					_JNODE_FREE_stackPush( _JNODE_FREE_nextChild );					
				} while( stackPos>=0 );
				return notesCount;
			}
	
#endif

