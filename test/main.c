#include "../jnode.h"                                                                      
																										
int main( void ){                                                                       
																						
	JNode* json = JNode_loadJSON( "example.json" );                                        
	if( !json ){ return 1; }                                                            
																						
	printf( "%s %s\n",                                                                  
		JNode_getString( json, "node1.array1[%i]", 1 ),                                      
		JNode_getString( json, "node1.array2[%i]", 1 )                                       
	);                                                                                  
																						
	return 0;                                                                           
} 
